CREATE SCHEMA multas;

CREATE TABLE multas.multasinfo (
  codmulta INT NOT NULL AUTO_INCREMENT,
  cpfcliente VARCHAR(11),
  placa VARCHAR(7),
  nomecliente VARCHAR(150),
  pontos INT,
  PRIMARY KEY (`codmulta`));
  
SELECT * FROM multas.multasinfo;