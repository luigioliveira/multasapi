const express = require('express');
const datetime = require('node-datetime');
const bodyParser = require('body-parser');
const db = require('./db/connection');
const port = 8000;
const server = 'Local/ProvaDoMaravilhosoDoGobbato'
const dt = datetime.create();
let formattedDate = dt.format('d/m/Y H:M:S');

let app = express();
app.use(bodyParser.json());

app.post('/multas', (req, res) => {
    let body = req.body;
    let cmd_insert = `INSERT INTO multasinfo (cpfcliente, placa, nomecliente, pontos) VALUES (?,?,?,?) `

    db.query(cmd_insert, [body.cpf, body.placa, body.nomecliente, body.pontos], (error, results) => {
        if(error){
            return res.status(400).json({error: error})
        }
        else {
            console.log("==============================================================");
            console.log("A new register was inserted on MultasInfo table.");
            console.log(`ID: ${results.insertId} | AffectedRows: ${results.affectedRows}`);
            console.log(`CPFClienteField: ${body.cpf}  | PlacaField: ${body.placa}`);
            console.log(`NomeCLienteField: ${body.nomecliente} | PontosField: ${body.pontos}`);
            console.log("==============================================================");
            return res.status(201).json({message: "Registro inserido com sucesso!", result: results})

        }
    })

});

app.get('/multas', (req,res) => {
    let body = req.body
    let cmd_select = 'SELECT * FROM multasinfo WHERE cpfcliente = ?'

    db.query(cmd_select, [body.cpf], (error,rows) => {
        if(error){
            return res.status(400).json({error: error});
        }
        else {
            return res.status(200).json(rows);
        }
    })
});

app.delete('/multas', (req, res) => {
    let body =  req.body
    let cmd_delete = 'DELETE FROM multasinfo WHERE placa = ?'

    db.query(cmd_delete, [body.placa], (error, results) => {
        if(error){
            return res.status(400).json({error: error});
        }
        else{
            console.log("==============================================================");
                console.log(`A register that has the ID ${body.id} and Placa ${body.placa} was deleted on MultasInfo table .`);
                console.log(`AffectedRows: ${results.affectedRows}`);
                console.log("==============================================================");
                return res.status(201).json({message: "Registro deletado com sucesso!", result: results})
        }
    })
})


app.listen(port, () => {
    console.log("==============================================================");
    console.log("Projeto API Multas por Luigi Oliveira iniciado!");
    console.log("Projeto executando na porta: " + port);
    console.log("Servidor: " + server + " | Horário: " + formattedDate);
    console.log("==============================================================");
});